#ifdef DEBUG
	#include <cstdio>
#endif

#include "Ds18b20.h"

Ds18b20::Ds18b20(uint8_t InPin)
		: Pin(InPin)
{
	gpio_init(Pin);
}

void Ds18b20::UpdateTemperature()
{
	Reset();

	if(!gpio_get(Pin))
	{
		sleep_us(240);
		WriteByte(0xCC);
		WriteByte(0x44);

		sleep_ms(1000);

		Reset();

		sleep_us(240);
		WriteByte(0xCC);
		WriteByte(0xBE);

		uint8_t Data1 = ReadByte();
		uint8_t Data2 = ReadByte();

		Temp = static_cast<int8_t>(((Data2 << 8) | Data1) * 0.0625);
	}
}

void Ds18b20::WriteByte(uint8_t InByte)
{
	for(uint8_t i = 0; i < 8; ++i)
	{
		bool Bit = static_cast<bool>(InByte & 0b00000001);
		WriteBit(Bit);
		InByte >>= 1;
	}
}

void Ds18b20::WriteBit(bool InBit) const
{
	gpio_set_dir(Pin, GPIO_OUT);
	gpio_put(Pin, false);

	if(InBit)
	{
		sleep_us(8);
		gpio_set_dir(Pin, GPIO_IN);
		sleep_us(53);
	}
	else
	{
		sleep_us(53);
		gpio_set_dir(Pin, GPIO_IN);
		sleep_us(8);
	}
}

uint8_t Ds18b20::ReadByte()
{
	uint8_t Result = 0;

	for(uint8_t i = 0; i < 8; ++i)
	{
		if(ReadBit()) { Result |= (1 << i); }

		sleep_us(45);
	}

	return Result;
}

bool Ds18b20::ReadBit() const
{
	gpio_set_dir(Pin, GPIO_OUT);
	gpio_put(Pin, false);
	sleep_us(10);
	gpio_set_dir(Pin, GPIO_IN);
	sleep_us(10);

	return gpio_get(Pin);
}

void Ds18b20::Reset() const
{
	gpio_set_dir(Pin, GPIO_OUT);
	gpio_put(Pin, false);
	sleep_us(480);

	gpio_set_dir(Pin, GPIO_IN);
	sleep_us(60);
}






