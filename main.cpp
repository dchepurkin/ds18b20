#include <cstdio>
#include "pico/stdlib.h"
#include <pico/multicore.h>
#include "Ds18b20.h"

Ds18b20 Temp(9);

void Core1()
{
	while(true)
	{
		Temp.UpdateTemperature();
		sleep_ms(1000);
	}
}

int main()
{
#ifdef DEBUG
	stdio_init_all();
#endif

	multicore_launch_core1(Core1);

	while(true)
	{
		printf("%i\n", Temp.GetTemperature());
		sleep_ms(1000);
	}
}
